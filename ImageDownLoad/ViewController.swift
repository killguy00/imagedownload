//
//  ViewController.swift
//  ImageDownLoad
//
//  Created by 엄기철 on 2016. 11. 5..
//  Copyright © 2016년 EOMKICHEOL. All rights reserved.
//

import UIKit

class ViewController: UIViewController, URLSessionDownloadDelegate {
    
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var progressView: UIProgressView!
    
    var downloadTask : URLSessionTask!
    
    @IBAction func downloadAction(_ sender: Any) {
        
        self.imgView.image = nil
        progressView.setProgress(0.0, animated: true)
        
        
        self.indicator.startAnimating()
        
        let sessionConfiguration = URLSessionConfiguration.default
        
        let session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        downloadTask = session.downloadTask(with: URL(string: "https://raw.githubusercontent.com/ChoiJinYoung/iphonewithswift2/master/sample.jpeg")!)
        
        
//        downloadTask = session.dataTask(with: URL(string: "https://raw.githubusercontent.com/ChoiJinYoung/iphonewithswift2/master/sample.jpeg")!, completionHandler: {(data, response, error) -> Void in
//            
//            self.imgView.image = UIImage(data: data!)
//            self.indicator.stopAnimating()
//        })
        
        
        downloadTask.resume()
    }
    @IBAction func suspendAction(_ sender: Any) {
        downloadTask.suspend()
        
    }
    
    @IBAction func resumeAction(_ sender: Any) {
        downloadTask.resume()
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
       
        if imgView != nil {
            imgView.image = nil
        }
        downloadTask.cancel()
        progressView.setProgress(0.0, animated: true)
        self.indicator.stopAnimating()
    }
    
    
    
        func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
    
            let data  = try! Data(contentsOf: location)
    
            self.imgView.image = UIImage(data: data)
            self.indicator.stopAnimating()
        }
    
    
    
        func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
    
            let progressStaus : Float = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
    
                progressView.setProgress(progressStaus, animated: true)
        }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

